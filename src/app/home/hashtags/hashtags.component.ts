import { Component, OnInit, Input } from '@angular/core';
import { Tag } from '../service/data.service';
import { Clipboard } from '@ionic-native/clipboard/ngx';

@Component({
  selector: 'app-hashtags',
  templateUrl: './hashtags.component.html',
  styleUrls: ['./hashtags.component.scss'],
})
export class HashtagsComponent implements OnInit {

  @Input() tags: Tag[] = [];

  constructor(private clipboard: Clipboard) { }

  ngOnInit() { }

  clickTag(tag: Tag) {
    const index = this.tags.findIndex(e => e.name === tag.name);
    if (index === -1) {
      this.tags.push(tag);
    } else {
      this.tags.splice(index, 1);
    }
  }

  copy() {
    console.log(this.toHashtags())
    this.clipboard.copy(this.toHashtags())
  }

  toHashtags() {
    return '#' + this.tags.map(e => e.name).sort((a, b) => 0.5 - Math.random()).join('#')
  }

}
