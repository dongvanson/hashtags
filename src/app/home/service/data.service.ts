import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(
    private http: HttpClient
  ) {

  }

  searchTag(tag) {
    return this.http.get('https://www.instagram.com/web/search/topsearch/', { params: { query: '#' + tag } }).pipe(
      map((e: any) => e.hashtags.map(x => ({
        name: x.hashtag.name,
        media_count: x.hashtag.media_count
      })).sort((a: Tag, b: Tag) => {
        return b.media_count - a.media_count
      }))
    )
  }

}

export interface Tag {
  name: string,
  media_count: number
}

//https://coolors.co/000000-fcfcfc-eeb4b3-f2f3ae-8cbde0
