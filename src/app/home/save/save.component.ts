import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInput } from '@ionic/angular';
import { Clipboard } from '@ionic-native/clipboard/ngx';

@Component({
  selector: 'app-save',
  templateUrl: './save.component.html',
  styleUrls: ['./save.component.scss'],
})
export class SaveComponent implements OnInit {

  @ViewChild('raw', { static: true }) ip: IonInput;

  constructor(
    private clipboard: Clipboard
  ) { }

  ngOnInit() { }

  random() {
    this.ip.getInputElement().then(i => {
      const tags = '#' + (i.value as string).split('#').filter(e => e).map(e => e.trim()).sort((a, b) => 0.5 - Math.random()).join('#');
      console.log(tags);
      this.clipboard.copy(tags);
    })
  }

}
