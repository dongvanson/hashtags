import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { HttpClient } from '@angular/common/http';
import { DataService, Tag } from './service/data.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  @Input() tags: Tag[] = [];
  currentPage = 'search';

  constructor(private clipboard: Clipboard, private http: HttpClient, private data: DataService) { }

  ngOnInit(): void {

  }

}
