import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { HttpClientModule } from '@angular/common/http';

import { HomePage } from './home.page';
import { SearchComponent } from './search/search.component';
import { HashtagsComponent } from './hashtags/hashtags.component';
import { SaveComponent } from './save/save.component';
import { DataService } from './service/data.service';
import { ParseIntPipe } from './pipe/parse-int.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  declarations: [HomePage, SearchComponent, HashtagsComponent, SaveComponent, ParseIntPipe],
  providers: [Clipboard, DataService]
})
export class HomePageModule { }
