import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { IonInput } from '@ionic/angular';
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { DataService, Tag } from '../service/data.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {

  @ViewChild('searchInput', { static: true }) searchInput: IonInput;
  @Input() tags: Tag[] = [];
  searchedTags: Tag[] = [];

  constructor(
    private data: DataService
  ) { }

  ngOnInit() {
    this.searchInput.getInputElement().then(input => {
      fromEvent(input, 'keyup').pipe(
        distinctUntilChanged(),
        debounceTime(500),
        map((e: any) => e.target.value.trim())
      ).subscribe(keyword => {
        this.data.searchTag(keyword).subscribe((tags: Tag[]) => {
          this.searchedTags = tags;
        })
      })
    })
  }

  clickTag(tag: Tag) {
    const index = this.tags.findIndex(e => e.name === tag.name);
    if (index === -1) {
      this.tags.push(tag);
    } else {
      this.tags.splice(index, 1);
    }
  }

  isSelected(tag: Tag) {
    return this.tags.findIndex(e => e.name === tag.name) === -1 ? false : true;
  }

  random() {
    const tempTags = this.searchedTags.concat().sort((a, b) => 0.5 - Math.random()).splice(0, 30);
    this.tags.splice(0, this.tags.length, ...tempTags);
  }

}
